package com.example.parq;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private EditText firstName, lastName, email, phoneNumber, password, confirmPassword;
    private Button btnRegister, btnLogin;
    private ProgressBar pbLoading;
    private static String URL_REGISTER="http://3.66.169.54/register.php";

    //JsonArrayRequest jsonArrayRequest;
    //RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstName=findViewById(R.id.txtFirstName);
        lastName=findViewById(R.id.txtLastName);
        email=findViewById(R.id.txtEmail);
        phoneNumber=findViewById(R.id.txtPhoneNumber);
        password=findViewById(R.id.txtPassword);
        confirmPassword=findViewById(R.id.txtConfirmPassword);
        btnRegister=findViewById(R.id.btnRegister);
        btnLogin=findViewById(R.id.btnLogin);
        pbLoading=findViewById(R.id.pbLoading);

        //disableButton(btnRegister);
        //btnRegister.setEnabled(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(RegisterActivity.this,  LogInActivity.class);
                startActivity(myIntent);
            }
        });


        /*if (!txtFirstName.matches("") && !lastName.isEmpty() && email.isEmpty() && !phoneNumber.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty())
            enableButton(btnRegister);*/
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                /*if (!firstName.isEmpty() && !lastName.isEmpty() && !email.isEmpty() && !phoneNumber.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty()) {
                    enableButton(btnRegister);
                }*/
                Register();
                Intent myIntent = new Intent(RegisterActivity.this,  ParkingActivity.class);
                startActivity(myIntent);
            }
        });
    }

    private void Register () {
        pbLoading.setVisibility(View.VISIBLE);
        btnRegister.setVisibility(View.GONE);

        final String firstName = this.firstName.getText().toString().trim();
        final String lastName = this.lastName.getText().toString().trim();
        final String email = this.email.getText().toString().trim();
        final String phoneNumber = this.phoneNumber.getText().toString().trim();
        final String password = this.password.getText().toString().trim();
        final String confirmPassword = this.confirmPassword.getText().toString().trim();
        //RequestQueue requestQueue = Volley.newRequestQueue(this);

        //if (confirmPassword.equals(password))
            //enableButton(btnRegister);
        /*if (checkInput(firstName) && checkInput(lastName) && checkInput(email) && checkInput(phoneNumber) && checkInput(password) && checkInput(confirmPassword))
        {
            enableButton(btnRegister);
        }*/
        if (!firstName.isEmpty() && !lastName.isEmpty() && !email.isEmpty() && !phoneNumber.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty()) {
            enableButton(btnRegister);
        }


                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            Toast.makeText(RegisterActivity.this, "Register Successful!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(RegisterActivity.this, "Register Error e! " + e.toString(), Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                        btnRegister.setVisibility(View.VISIBLE);

                    }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Register Error error! " + error.toString(), Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                        btnRegister.setVisibility(View.VISIBLE);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("firstName", firstName);
                params.put("lastName", lastName);
                params.put("email", email);
                params.put("phoneNumber", phoneNumber);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    void enableButton(final Button btn){
        btn.setEnabled(true);
        btn.setTextColor(Color.parseColor("#ffffff"));
    }

    //Dezactivare buton Confirm daca nu e ales niciun loc
    void disableButton(final Button btn){
        btn.setEnabled(false);
        btn.setTextColor(Color.parseColor("#bfbfbf"));
    }

    /*boolean checkInput (String data) {
        if (!data.isEmpty())
            return true;
    }*/

}