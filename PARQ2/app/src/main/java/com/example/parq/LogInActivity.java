package com.example.parq;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogInActivity extends AppCompatActivity {

    private EditText email, password;
    private Button btnSignIn, btnCreateAccount;
    private static String URL_LOGIN="http:/3.66.169.54/login.php";
    private ProgressBar pbLoading;

    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        sessionManager = new SessionManager(this);

        email=findViewById(R.id.txtEmailAddress);
        password=findViewById(R.id.txtAccountPassword);
        btnSignIn=findViewById(R.id.btnSignIn);
        btnCreateAccount=findViewById(R.id.btnCreateAccount);
        pbLoading=findViewById(R.id.pbLoading);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mEmail = email.getText().toString().trim();
                String mPassword = password.getText().toString().trim();

                if (mEmail.isEmpty() || mPassword.isEmpty()) {
                    email.setError("Please insert your email address!");
                    password.setError("Please insert your password!");
                } else {
                    LogIn(mEmail, mPassword);
                    //Intent myIntent = new Intent(LogInActivity.this,  ParkingActivity.class);
                    //startActivity(myIntent);
                }


            }
        });

       btnCreateAccount.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent myIntent = new Intent(LogInActivity.this,  RegisterActivity.class);
               startActivity(myIntent);
           }
       });
    }

    private void LogIn(final String email, final String password) {
        //pbLoading.setVisibility(View.VISIBLE);
        //btnSignIn.setVisibility((View.GONE));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("login");

                            if (success.equals("1")) {
                                for (int i=0; i<jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String firstName = object.getString("firstName").trim();
                                    String email = object.getString("email").trim();
                                    String id = object.getString("id").trim();

                                    sessionManager.createSession(firstName, email, id);

                                    //Toast.makeText(LogInActivity.this, "Success Login!", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(LogInActivity.this, "Success Login, " + firstName + "\n"
                                            + email, Toast.LENGTH_SHORT).show();
                                    Intent myIntent = new Intent(LogInActivity.this,  HomeActivity.class);
                                    myIntent.putExtra("firstName", firstName);
                                    myIntent.putExtra("email", email);
                                    startActivity(myIntent);
                                    finish();
                                    //pbLoading.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //pbLoading.setVisibility(View.GONE);
                            btnSignIn.setVisibility(View.VISIBLE);
                            Toast.makeText(LogInActivity.this, "Error e! " + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //pbLoading.setVisibility(View.GONE);
                        btnSignIn.setVisibility(View.VISIBLE);
                        Toast.makeText(LogInActivity.this, "Error error! " + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}